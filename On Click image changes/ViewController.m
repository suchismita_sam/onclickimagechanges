//
//  ViewController.m
//  On Click image changes
//
//  Created by Click Labs134 on 9/15/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
UIImageView *imageOne;
UIImageView *imageTwo;
UIImageView *imageThree;
UIImageView *imageFour;
UIImageView *imageFive;
UIButton *nextImage;
UIButton *previousImage;
UIImageView *viewImage;
NSArray *imageArray;
int indeximage;
UIImageView *backgroundImage;


- (void)viewDidLoad {
    [super viewDidLoad];
    //Next Image to be shown
  //  UIButton *nextImage=[UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    UIButton *nextImage=[[UIButton alloc]init];
    nextImage.frame=CGRectMake(270, 450, 40, 40);
 //   [nextImage setTitle:@">" forState:UIControlStateNormal];
    nextImage.backgroundColor=[UIColor blackColor];
    [nextImage setFont:[UIFont boldSystemFontOfSize:30]];
    [nextImage setBackgroundImage:[UIImage imageNamed:
                                   @"right.jpg"]forState:UIControlStateNormal];

[nextImage addTarget:self action:@selector(nextPicture:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:nextImage];
    
    //Previous Image
    previousImage=[[UIButton alloc]init];
    previousImage.frame=CGRectMake(10, 450, 40, 40);
//   [previousImage setTitle:@"<" forState:UIControlStateNormal];
   previousImage.backgroundColor=[UIColor blackColor];
    [previousImage setFont:[UIFont boldSystemFontOfSize:30]];
    [previousImage setBackgroundImage:[UIImage imageNamed:
                                   @"left.jpg"]forState:UIControlStateNormal];
    [previousImage addTarget:self action:@selector(previousPicture:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:previousImage]; 

    //backgroundImage
    UIImageView *backgroundImage=[[UIImageView alloc]init];
    backgroundImage.frame= CGRectMake(50,50,100,100);
    backgroundImage.image = [UIImage imageNamed:@"1-iPhone-5-Wallpaper-Apple-glass-applelogo.jpeg"];
     [backgroundImage setContentMode:UIViewContentModeScaleAspectFit];
    self.view.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"1-iPhone-5-Wallpaper-Apple-glass-applelogo.jpeg"]];

    //By default Image shown
    viewImage=[[UIImageView alloc]init];
    viewImage.frame= CGRectMake(65,65,200,350);
    viewImage.image = [UIImage imageNamed:@"1.jpg"];
    [self.view addSubview:viewImage];
    
    //defining array
   
    imageArray=@[@"1.jpg",@"images-3.jpg",@"images-4.jpg",@"images-5.jpg"];
    // Do any additional setup after loading the view, typically from a nib.
}
-(void) nextPicture:(UIButton *)sender

{
   // NSUInteger count=[imageArray count];
    if (indeximage<=imageArray.count)
    {
        viewImage.image = [UIImage imageNamed:[imageArray objectAtIndex :indeximage]];
        indeximage++;
    }
}

-(void) previousPicture:(UIButton *)sender

{
   // NSUInteger count=[imageArray count];
    if (indeximage > 0)
    {
        indeximage-- ;
        viewImage.image = [UIImage imageNamed:[imageArray objectAtIndex :indeximage]];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
